package main

import (
	"encoding/json"
	"fmt"
	"gitlab.com/rhyme80/golang-mysql/src/config"
	"gitlab.com/rhyme80/golang-mysql/src/entities"
	"gitlab.com/rhyme80/golang-mysql/src/models"
	"io/ioutil"
)

func main() {
	db, err := config.GetMySqlDB()
	if err != nil {
		fmt.Println(err)
	}

	fileJson, err := ioutil.ReadFile("./cts.json")
	if err != nil {
		fmt.Println(err)
	}

	var objs []entities.Cts

	err = json.Unmarshal(fileJson, &objs)
	if err != nil {
		fmt.Println(err)
	}

	inserts, err := models.CtsModel.InsertarCuentas(&objs)
}
