package entities

type Cts struct {
	Id                     int64
	EntidadBancaria        string
	TipoCuenta             string
	NroCuenta              string
	NroCuentaInterbancaria string
}
