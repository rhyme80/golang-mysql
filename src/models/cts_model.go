package models

import (
	"database/sql"
	"gitlab.com/rhyme80/golang-mysql/src/entities"
)

type CtsModel struct {
	Db *sql.DB
}

func (c CtsModel) InsertarCuentas(lista []entities.Cts) (bool, error) {
	insForm, err := c.Db.Prepare("CALL sys_rrhh_spu_actualizar_cts(?)")
	if err != nil {
		panic(err.Error())
	}
	_, err = insForm.Exec(lista)
	if err != nil {
		return false, err
	}
	return true, err
}
